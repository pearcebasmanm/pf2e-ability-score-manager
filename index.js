Hooks.on("renderActorSheet", (sheet, $html) => {
    const $button = $(
        '<a class="item-edit" style="float: right; position: relative; right: 3px; top: -2px;"><i class="fas fa-edit"></i></a>'
    ).on("click", () => {
        globalThis.ABS = sheet.actor;
        if (sheet.actor.ancestry && sheet.actor.background && sheet.actor.class) {
            new Application({
                width: 400 + 20 * sheet.actor.data.data.details.level.value,
                height: 385,
                popOut: true,
                minimizable: true,
                resizable: false,
                id: "rule-element-generator",
                template: "modules/pf2e-abs-manager/application.html",
                title: "Ability Score Manager",
            }).render(true);
        } else {
            ui.notifications.info("PF2E Ability Score Manager | Please Choose Ancestry, Background, and Class");
        }
    });
    $html.find("h3:contains('Ability Scores')").append($button);
});
